package com.chinacreator.cases.action;

import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import org.springframework.beans.factory.InitializingBean;
import org.springframework.stereotype.Component;

import com.bstek.urule.model.ExposeAction;
import com.bstek.urule.model.library.action.annotation.ActionBean;
import com.chinacreator.cases.bean.CaseInfo;

import lombok.extern.slf4j.Slf4j;

@ActionBean(name = "CaseLoader")
@Component("CaseLoader")
@Slf4j
public class CaseLoader implements InitializingBean {

	@Override
	public void afterPropertiesSet() throws Exception {
		log.info("初始化数据源");
	}
	
	@ExposeAction(id="randomExtractCases",parameters = {"案卷立案时间","案卷结案时间","案卷类型","案卷状态","执法主体","执法依据","执法职权","办案人员","抽查方式","抽取数量","监督状态"})
	public List<CaseInfo> randomExtractCases(String createDate,String closeDate,String caseType,String caseStatus, String orgCode,String accordingLow,String authority,String userCode,String randomRule,
			Integer count,String superviseStatus) {
		List<CaseInfo>  eventList = new ArrayList<>();
		count = count == null ? 10 : count;
		String[] authoritys = new String[] {"001","002","003"};
		String[] caseStatuses = new String[] {"01","02","03","04"}; //立案，撤销立案，办理中，已办结
		String[] caseTypes = new String[] {"01","02","03"}; //
		String[] changeReasonStatuses = new String[] {"00","01"}; //未更改案由，已更改案由
		String[] delayStatuses = new String[] {"00","01"}; //未延期，已延期
		String[] orgCodes = new String[] {"0001","0002","0004","0006"}; //未过期，已过期
		String[] useCodes = new String[] {"admin","zhangxia","yanglian","lipeng"}; //未过期，已过期
		String[] dueStatuses = new String[] {"00","01"}; //未过期，已过期
		String[] reductionStatuses = new String[] {"00","01"}; //未减免，已减免
		String[] superviseStatuses = new String[] {"00","01"}; //未监督，已监督
		
 		SecureRandom radom = new SecureRandom();
		for(int i = 0 ;i<count;i++) {
			CaseInfo caseInfo = CaseInfo.builder()
					.id(UUID.randomUUID().toString().substring(0,8))
					.accordingLow("支付依据")
					.authority(authoritys[Math.abs(radom.nextInt())%authoritys.length])
					.caseStatus(caseStatuses[Math.abs(radom.nextInt())%caseStatuses.length])
					.caseType(caseTypes[Math.abs(radom.nextInt())%caseTypes.length])
					.changeReasonStatus(changeReasonStatuses[Math.abs(radom.nextInt())%changeReasonStatuses.length])
					.closeDate("2023-10-0"+(i%10+1))
					.createDate("2023-09-0"+(i%10+1))
					.delayDays((8*i%10+1))
					.delayStatus(delayStatuses[Math.abs(radom.nextInt())%delayStatuses.length])
					.dueDays(8*i%10+1)
					.dueStatus(dueStatuses[Math.abs(radom.nextInt())%dueStatuses.length])
					.orgCode(orgCodes[Math.abs(radom.nextInt())%orgCodes.length])
					.reductionStatus(reductionStatuses[Math.abs(radom.nextInt())%reductionStatuses.length])
					.superviseStatus(superviseStatuses[Math.abs(radom.nextInt())%superviseStatuses.length])
					.userCode(useCodes[Math.abs(radom.nextInt())%useCodes.length])
			.build();
			eventList.add(caseInfo);
		}
		return eventList;
	}

}
