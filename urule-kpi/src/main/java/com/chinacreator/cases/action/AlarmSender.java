package com.chinacreator.cases.action;

import org.springframework.beans.factory.InitializingBean;
import org.springframework.stereotype.Component;

import com.bstek.urule.model.ExposeAction;
import com.bstek.urule.model.library.action.annotation.ActionBean;

import lombok.extern.slf4j.Slf4j;

@ActionBean(name = "AlarmSender")
@Component("AlarmSender")
@Slf4j
public class AlarmSender implements InitializingBean {

	@Override
	public void afterPropertiesSet() throws Exception {
		log.info("初始化数据源");
	}
	
	@ExposeAction(id="sendAlarm",parameters = {"案件id","用户code","报警类型"})
	public String sendAlarm(String caseId,String userCode,String alarmType) {
		log.error(String.format("发送预警，案件id=%s, 接收人=%s,预警类型=%s",caseId,userCode,alarmType));
		System.out.println(String.format("发送预警，案件id=%s, 接收人=%s,预警类型=%s",caseId,userCode,alarmType));
		return "success";
	}

}
