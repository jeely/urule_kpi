package com.chinacreator.cases.bean;

import com.bstek.urule.model.Label;

import lombok.Builder;
import lombok.Data;

@Builder
@Data
public class CaseInfo {
	@Label("案件id")
	private String id;
	
	@Label("案卷立案时间")
	private String createDate;
	@Label("案卷结案时间")
	private String closeDate;

	@Label("减轻与免除处罚标记")
	private String reductionStatus;

	@Label("变更案由状态")
	private String changeReasonStatus;

	@Label("过期状态")
	private String dueStatus;
	@Label("过期天数")
	private Integer dueDays;

	@Label("延期状态")
	private String delayStatus;
	@Label("延期天数")
	private Integer delayDays;

	@Label("案卷类型")
	private String caseType;
	@Label("案卷状态")
	private String caseStatus;
	@Label("执法主体")
	private String orgCode;
	@Label("执法依据")
	private String accordingLow;
	@Label("执法职权")
	private String authority;
	@Label("办案人员")
	private String userCode;
	@Label("监督状态")
	private String superviseStatus;
	@Label("预警类型")
	private String alarmType;
}
