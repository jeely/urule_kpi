package com.chinacreator.cases.bean;

import java.util.List;

import com.bstek.urule.model.Label;

import lombok.Builder;
import lombok.Data;

@Builder
@Data
public class CaseVariable {
	@Label("事件集合")
	private List<CaseInfo> caseList;
}
