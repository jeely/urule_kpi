package com.chinacreator.kpi.enums;

public enum EventType {
	CaseClose("案件结案"), absence("缺勤");

	private String label;

	private EventType(String label) {
		this.label = label;
	}

	public String getLabel() {
		return label;
	}
}
