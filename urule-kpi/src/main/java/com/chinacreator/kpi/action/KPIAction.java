package com.chinacreator.kpi.action;

import org.springframework.beans.factory.InitializingBean;
import org.springframework.stereotype.Component;

import com.bstek.urule.model.ExposeAction;
import com.bstek.urule.model.library.action.annotation.ActionBean;

import lombok.extern.slf4j.Slf4j;

@ActionBean(name = "KPIAction")
@Component("KPIAction")
@Slf4j
public class KPIAction implements InitializingBean {
	@Override
	public void afterPropertiesSet() throws Exception {
		log.info("初始化数据源");
	}

	@ExposeAction(id="hello")
	public String hello() {
		return "hello";
	}

	@ExposeAction(id="count",parameters = {"用户账号","事件分类","事件类型","开始时间","结束时间"})
	public int count(String userAccount, String category, String eventType, String startDate, String endDate) {
		
		return 1;
	}


}
