package com.chinacreator.kpi.action;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.InitializingBean;
import org.springframework.stereotype.Component;

import com.bstek.urule.model.ExposeAction;
import com.bstek.urule.model.library.action.annotation.ActionBean;
import com.chinacreator.kpi.bean.KPIEvent;

import lombok.extern.slf4j.Slf4j;

@ActionBean(name = "KPIDataLoader")
@Component("KPIDataLoader")
@Slf4j
public class KPIDataLoader implements InitializingBean {

	@Override
	public void afterPropertiesSet() throws Exception {
		log.info("初始化数据源");
	}
	/**
	 * 比如 
	 * 		加载用户A在一段时间内的出勤次数
	 * @param targetType
	 * @param target
	 * @param eventCatetory
	 * @param eventType
	 * @param startDate
	 * @param endDate
	 * @return
	 */
	@ExposeAction(id="count",parameters = {"对象类型","对象code","事件分类","事件类型","开始时间","结束时间"})
	public Long load(String targetType,String target,String eventCatetory,String eventType,String startDate, String endDate) {
		
		return 0L;
	}
	
	@ExposeAction(id="loadEvents",parameters = {"对象类型","对象code","事件分类","开始时间","结束时间"})
	public List<KPIEvent> loadEvents(String targetType,String target,String eventCatetory,String startDate, String endDate) {
		List<KPIEvent>  eventList = new ArrayList<>();
		for(int i = 0 ;i<5;i++) {
			String type = eventCatetory+"0"+(i%5+1);
			KPIEvent event = KPIEvent.builder()
			.businessId(String.valueOf(i))
			.category(eventCatetory)
			.eventType(type)
			.happendedDate("2023-04-0"+(i%9+1))
			.departcode("")
			.id(String.valueOf(i))
			.count(type.endsWith("01")?20:1)
			.org_code(target)
			.regionCode("")
			.user_account(target).build();
			eventList.add(event);
		}
		return eventList;
	}

}
