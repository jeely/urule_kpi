package com.chinacreator.kpi.bean;

import java.util.List;

import com.bstek.urule.model.Label;

import lombok.Builder;
import lombok.Data;

@Builder
@Data
public class KPIEventVariable {
	/**
	 * 用户账号
	 */
	@Label("用户账号")
	private String user_account;

	@Label("事件集合")
	private List<KPIEvent> eventList;

	/**
	 * 事件分类： 案件、考勤等
	 */
	@Label("事件分类")
	private String category;

	/**
	 * 事件类型： 案件--结案 考勤--出勤等
	 */
	@Label("事件类型")
	private String eventType;
	
	/**
	 * 发生时间 (yyyy-MM-dd)
	 */
	@Label("发生时间")
	private String happendedDate;
	
	/**
	 * 所属区域
	 */
	@Label("所属区域")
	private String regionCode;
	
	@Label("分数")
	private Integer score;
}
