package com.chinacreator.kpi.bean;

import com.bstek.urule.model.Label;

import lombok.Builder;
import lombok.Data;

@Builder
@Data
public class KPIEvent {

	/**
	 * 事件主键id
	 */
	@Label("id")
	private String id;

	/**
	 * 事件分类： 案件、考勤等
	 */
	@Label("事件分类")
	private String category;

	/**
	 * 事件类型： 案件--结案 考勤--出勤等
	 */
	@Label("事件类型")
	private String eventType;

	/**
	 * 指向的业务id
	 */
	@Label("业务id")
	private String businessId;

	/**
	 * 用户账号
	 */
	@Label("用户账号")
	private String user_account;

	/**
	 * 机构编码
	 */
	@Label("机构编码")
	private String org_code;

	/**
	 * 部门编码
	 */
	@Label("部门编码")
	private String departcode;

	/**
	 * 发生时间 (yyyy-MM-dd)
	 */
	@Label("发生时间")
	private String happendedDate;
	
	/**
	 * 所属区域
	 */
	@Label("所属区域")
	private String regionCode;
	
	/**
	 * 数量
	 */
	@Label("数量")
	private Integer count;
	
	/**
	 * 得分
	 */
	@Label("得分")
	private Integer score;

}
