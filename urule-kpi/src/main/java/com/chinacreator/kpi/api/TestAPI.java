package com.chinacreator.kpi.api;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.bstek.urule.Utils;
import com.bstek.urule.runtime.KnowledgePackage;
import com.bstek.urule.runtime.KnowledgeSession;
import com.bstek.urule.runtime.KnowledgeSessionFactory;
import com.bstek.urule.runtime.response.FlowExecutionResponse;
import com.bstek.urule.runtime.service.KnowledgeService;
import com.chinacreator.kpi.bean.KPIEventVariable;

@RestController
public class TestAPI  implements InitializingBean{
	
	@Autowired
	private KnowledgeService service;

	@GetMapping(path = "/v1/api/test")
	public @ResponseBody Object test() throws Exception {
		// 从Spring中获取KnowledgeService接口实例
		KnowledgeService service = (KnowledgeService) Utils.getApplicationContext().getBean(KnowledgeService.BEAN_ID);
		// 通过KnowledgeService接口获取指定的资源包"projectName/test123"
		KnowledgePackage knowledgePackage = service.getKnowledge("考核项目/com.chinacreator.kpi.chuqing2");
		// 通过取到的KnowledgePackage对象创建KnowledgeSession对象
		KnowledgeSession session = KnowledgeSessionFactory.newKnowledgeSession(knowledgePackage);
		KPIEventVariable employee = KPIEventVariable.builder().user_account("admin").build();
		// 将业务数据对象Employee插入到KnowledgeSession中
		session.insert(employee);
		Map<String, Object> parameters = new HashMap<String, Object>();

		parameters.put("totalScore", 0);
		parameters.put("startDate", "2023-04-01");
		parameters.put("endDate", "2023-04-31");
		parameters.put("currentUser", "admin");
		parameters.put("currentOrg", "001");
		parameters.put("currentDept", "001001");
		parameters.put("currentRegion", "4301");
		parameters.put("eventType", "0201");
		parameters.put("eventCategory", "02");

		FlowExecutionResponse  response2 = session.startProcess("test4", parameters);
		
		// 执行所有满足条件的规则
//		RuleExecutionResponse response = session.fireRules(parameters);
		
		List<Object> list = session.getAllFacts();
		
		Map<String, Object> result = session.getParameters();
		System.out.println(result);

		Map<String,Object> r = new HashMap<>();
		
		r.put("parameters", result);
		r.put("variables", employee);
		
		return r;
	}

	@Override
	public void afterPropertiesSet() throws Exception {
		System.out.println(service);
		
	}
}
